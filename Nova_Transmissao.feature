# language: pt

Funcionalidade: Nova Transmissão
Como um usúario do WhatsApp
Eu quero enviar uma imagem simultaneamente
Para diversos contatos da lista

@regression @ntransmissao
Cenário: Criar lista de transmissão
    Dado que estou no WhatsApp
    E possuo uma lista de contatos
    Quando seleciono uma nova transmissão
    E escolho 2 contatos da lista
    Então a tela de conversa deve ser apresentada com os 2 contatos escolhidos

@regression @ntransmissao
Cenário: Enviar Imagem
    Dado que estou no WhatsApp
    E seleciono uma lista de transmissão criada
    Quando escolho uma imagem
    E envio a imagem
    Então a imagem deve ser apresentada como enviada para ambos contatos

@regression @ntransmissao
Cenário: Deletar Imagem
    Dado que estou no WhatsApp
    E seleciono uma lista de transmissão criada com uma imagem enviada
    Quando seleciono a imagem desejada
    E excluo a imagem
    Então a imagem deve ser removida da lista de transmissão para ambos contatos