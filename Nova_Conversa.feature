# language: pt

Funcionalidade: Nova Conversa
Como um usúario do WhatsApp
Eu quero localizar um contato existente
Para realizar uma nova conversa

@regression @nconversa
Cenário: Escolher contato
    Dado que estou no WhatsApp
    E possuo uma lista de contatos
    Quando seleciono um contato
    Então a tela de conversa do contato deve ser apresentada

@regression @nconversa
Cenário: Enviar Mensagem
    Dado que estou no WhatsApp
    E possuo um contato selecionado
    Quando entro com a mensagem desejada
    E envio a mensagem
    Então a mensagem deve ser apresentada como enviada

@regression @nconversa
Cenário: Deletar Mensagem
    Dado que estou no WhatsApp
    E estou na tela de conversa do contato que enviei a mensagem
    Quando seleciono a mensagem enviada
    E excluo a mensagem
    Então a mensagem deve ser removida da tela de conversa do contato